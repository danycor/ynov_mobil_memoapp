package com.danycor.memoapp;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.danycor.memoapp.memo.Memo;
import com.danycor.memoapp.memo.MemoDAO;

@Database(entities = {Memo.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MemoDAO memoDao();
}
