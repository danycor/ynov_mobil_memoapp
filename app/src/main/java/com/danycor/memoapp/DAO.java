package com.danycor.memoapp;

import android.content.Context;

import androidx.room.Room;

class DAO {
    private static AppDatabase appDatabase = null;

    private DAO() {
    }

    static AppDatabase getInstance(Context context) {
        if (DAO.appDatabase == null) {
            DAO.appDatabase = Room.databaseBuilder(context, AppDatabase.class, "accounts")
                    .allowMainThreadQueries().build();
        }
        return DAO.appDatabase;
    }
}
