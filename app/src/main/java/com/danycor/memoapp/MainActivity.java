package com.danycor.memoapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.danycor.memoapp.memo.Memo;
import com.danycor.memoapp.memo.MemoAdapter;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    public final static String LAST_POSITION_KEY = "last_position_key";

    private List<Memo> dataset;
    private MemoAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataset = this.getDataSet();
        this.setUpRecyclerView();

        findViewById(R.id.main_button_add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText et = findViewById(R.id.main_field);
                String text = et.getText().toString();
                addNewMemo(text);
                toastMemoAdd(text);
                et.setText("");
            }
        });

        this.toastLastPosition();
    }

    protected void addNewMemo(String text) {
        Memo memo = new Memo(text);
        DAO.getInstance(getApplicationContext()).memoDao().insertAll(memo);
        dataset.clear();
        dataset.addAll(DAO.getInstance(getApplicationContext()).memoDao().getAll());
        mAdapter.notifyDataSetChanged();
    }

    protected void setUpRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.main_list_memo);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new MemoAdapter(dataset, this);
        recyclerView.setAdapter(mAdapter);
    }

    protected void toastLastPosition() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        int value = sp.getInt(MainActivity.LAST_POSITION_KEY, -1);
        if (value >= 0) {
            Toast toast = Toast.makeText(this, getResources().getString(R.string.main_memo_last_position_clic_text) + value, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    protected void toastMemoAdd(String text) {
        Toast toast = Toast.makeText(this, getResources().getString(R.string.main_memo_add_text) + text, Toast.LENGTH_SHORT);
        toast.show();
    }

    protected List<Memo> getDataSet() {
        List<Memo> list = new ArrayList<>();
        List<Memo> datas = DAO.getInstance(getApplicationContext()).memoDao().getAll();
        list.clear();
        list.addAll(datas);
        return list;
    }
}
