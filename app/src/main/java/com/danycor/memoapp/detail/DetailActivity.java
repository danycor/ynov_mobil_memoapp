package com.danycor.memoapp.detail;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.danycor.memoapp.R;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_layout);

        if (findViewById(R.id.detail_frame_layout) != null) {
            if (savedInstanceState != null) {
                return;
            }
            DetailFragment detailFragment = new DetailFragment();
            detailFragment.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.detail_frame_layout, detailFragment).commit();
        }

    }
}
