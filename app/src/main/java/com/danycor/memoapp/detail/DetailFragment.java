package com.danycor.memoapp.detail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.danycor.memoapp.R;

public class DetailFragment extends Fragment {
    public final static String TEXT_FIELD_KEY = "TEXT_FIELD_KEY";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.detail_fragment, container, false);
        if (getArguments() != null) {
            String detail = getArguments().getString(DetailFragment.TEXT_FIELD_KEY);
            ((TextView) v.findViewById(R.id.detail_memo)).setText(detail);
        }
        return v;
    }
}
