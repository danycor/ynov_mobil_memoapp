package com.danycor.memoapp.memo;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "memo")
public class Memo {
    @PrimaryKey(autoGenerate = true)
    public long memoId;

    public String text;

    Memo() {

    }

    public Memo(String note) {
        this.text = note;
    }
}
