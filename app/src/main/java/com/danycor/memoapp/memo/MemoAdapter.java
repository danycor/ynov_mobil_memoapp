package com.danycor.memoapp.memo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.danycor.memoapp.MainActivity;
import com.danycor.memoapp.R;
import com.danycor.memoapp.detail.DetailActivity;
import com.danycor.memoapp.detail.DetailFragment;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.UnsupportedEncodingException;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class MemoAdapter extends RecyclerView.Adapter<MemoAdapter.MemoViewHolder> {
    private List<Memo> dataset;
    private AppCompatActivity activity;

    static class MemoViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ConstraintLayout memoView;
        MemoAdapter adapter;
        String detailText;

        MemoViewHolder(ConstraintLayout v, final MemoAdapter adapter) {
            super(v);
            this.adapter = adapter;
            memoView = v;
            memoView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    adapter.savePosition(getAdapterPosition());
                    // call web service
//                    adapter.callWebService(text);
                    adapter.showDetail(detailText);
                }
            });
        }

        void fillElement(Memo memo) {
            detailText = memo.text;
            String text = memo.text.length() > 20 ? memo.text.substring(0, 20) + " ..." : memo.text;
            ((TextView) memoView.findViewById(R.id.memo_text)).setText(text);
        }
    }

    public MemoAdapter(List<Memo> dataset, AppCompatActivity activity) {
        this.activity = activity;
        this.dataset = dataset;
    }

    @NonNull
    @Override
    public MemoViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.memo_layout, parent, false);
        return new MemoViewHolder(v, this);

    }

    @Override
    public void onBindViewHolder(@NonNull MemoViewHolder holder, int position) {
        holder.fillElement(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    private void savePosition(int position) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(activity);
        SharedPreferences.Editor e = sp.edit();
        e.putInt(MainActivity.LAST_POSITION_KEY, position);
        e.apply();
    }

    private void callWebService(String memoText) {
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        params.put("text", memoText);
        client.post("http://httpbin.org/post", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                StringBuilder message = new StringBuilder();
                Gson gson = new Gson();
                try {
                    String jsonString = new String(response, "UTF-8");
                    WsReturn datas = gson.fromJson(jsonString, WsReturn.class);
                    message.append(datas.form.text);
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    message.append(R.string.http_fail);
                    message.append(e.getMessage());
                }
                Toast toast = Toast.makeText(activity, message.toString(), Toast.LENGTH_SHORT);
                toast.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                StringBuilder message = new StringBuilder();
                message.append(R.string.http_fail);
                message.append(statusCode);
            }
        });
    }

    private void showDetail(String detail) {
        // compare size
        if (getScreenWidth() > 512) {
            DetailFragment detailFragment = new DetailFragment();
            Intent i = new Intent();
            i.putExtra(DetailFragment.TEXT_FIELD_KEY, detail);
            detailFragment.setArguments(i.getExtras());
            activity.getSupportFragmentManager().beginTransaction()
                    .add(R.id.detail_frame_layout, detailFragment).commit();
        } else {
            Intent i = new Intent(activity, DetailActivity.class);
            i.putExtra(DetailFragment.TEXT_FIELD_KEY, detail);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.startActivity(i);
        }
    }

    private int getScreenWidth() {
        WindowManager wm = (WindowManager) activity.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics metrics = new DisplayMetrics();
        if (wm != null) {
            Display display = wm.getDefaultDisplay();
            display.getMetrics(metrics);
        }
        return metrics.widthPixels;
    }
}
