package com.danycor.memoapp.memo;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface MemoDAO {
    @Query("SELECT * FROM memo")
    List<Memo> getAll();

    @Query("SELECT * FROM memo WHERE memoId IN (:memoIds)")
    List<Memo> loadAllByIds(int[] memoIds);

    @Query("SELECT * FROM memo WHERE text LIKE :text LIMIT 1")
    Memo findByText(String text);

    @Insert
    void insertAll(Memo... memo);

    @Delete
    void delete(Memo memo);
}
